import pygame
import math


pygame.init()
sc = pygame.display.set_mode((600, 400))
font = pygame.font.Font(None, 36)
last_tick = 0

rectangle = {
    "color": (125, 125, 125),
    "size": {
        "width": 200,
        "height": 300,
    },
    "position": {
        "x": 100,
        "y": 100,
    },
}
obj_rectangle = pygame.Rect((
        rectangle["position"]["x"],
        rectangle["position"]["y"],
        rectangle["size"]["width"],
        rectangle["size"]["height"],
))

line1 = ((40, 100), (140, 300))

lines = ((50, 120), (20, 220), (300, 150))

polygon = ((80, 20), (60, 150), (200, 150), (300, 80))

circle = ((300, 300), 50)

ellipse = (400, 100, 100, 50)

arc = (400, 200, 200, 200)

play = True
while play:
    ticks = pygame.time.get_ticks() - last_tick
    last_tick = pygame.time.get_ticks()
    fps = font.render(str(1000 / ticks) if ticks > 0 else "NaN",
    1, (0, 255, 0))

    sc.fill((0, 0, 0))
    sc.blit(fps, (0, 0))

    pygame.draw.rect(sc, rectangle["color"], obj_rectangle)

    pygame.draw.line(sc, (255, 0, 0), *line1, 25)

    pygame.draw.lines(sc, (255, 255, 255), True, lines, 20)

    pygame.draw.polygon(sc, (0, 0, 255), polygon)
    
    pygame.draw.circle(sc, (0, 255, 0), *circle)

    pygame.draw.ellipse(sc, (128, 255, 255), ellipse)

    pygame.draw.arc(sc, (0, 128, 128), arc, 0, math.pi/4, 10)

    pygame.display.update()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            play = False
            pygame.quit()
            break

    pygame.time.delay(20)