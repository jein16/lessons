from random import randint

import pygame


YELLOW = (255, 255, 0)
RED = (255, 0, 0)

class Game:
    def __init__(self, fps = 60, show_fps=False):
        self.player = pygame.Rect(375, 750, 50, 50)
        self.player_speed = (0, 0)
        self.points = 0
        self.coin = pygame.Rect(
            randint(0, 775),
            randint(0, 775),
            25,
            25,
        )
        self.play = True
        self.fps = fps
        self.clock = pygame.time.Clock()

        pygame.init()
        self.font = pygame.font.Font(None, 36)
        self.screen = pygame.display.set_mode((800, 800))
        
    def loop(self):
        while self.play:
            self.draw()
            self.clock.tick(self.fps)
            self.handle()
            self.update()

    def draw(self):
        self.screen.fill((0, 0, 0))
        
        pygame.draw.rect(self.screen, YELLOW, self.player)
        pygame.draw.rect(self.screen, RED, self.coin)
        pygame.draw.rect(self.screen, (128, 128, 128), (0, 0, 800, 800), 10)

        points_text = self.font.render(f"Points: {self.points}", 0, (0, 255, 0))
        self.screen.blit(points_text, (600, 0))

        pygame.display.update()

    def handle(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.play = False
                    pygame.quit()
                    break
                elif event.key == pygame.K_DOWN:
                    self.player_speed = (0, 10)
                elif event.key == pygame.K_UP:
                    self.player_speed = (0, -10)                    
                elif event.key == pygame.K_LEFT:
                    self.player_speed = (-10, 0)
                elif event.key == pygame.K_RIGHT:
                    self.player_speed = (10, 0)


    def update(self):
        condition = self.player.right <= 790 or self.player_speed[0] <= 0
        condition = condition and (self.player.left >= 10 or self.player_speed[0] >= 0)
        condition = condition and (self.player.top >= 10 or self.player_speed[1] >= 0)
        condition = condition and (self.player.bottom <= 790 or self.player_speed[1] <= 0)


        if condition:
            self.player = self.player.move(*self.player_speed) 

        if self.player.colliderect(self.coin):
            self.points += 1
            self.coin = pygame.Rect(
            randint(0, 775),
            randint(0, 775),
            25,
            25,
        )

game = Game()
game.loop()