# __iter__
# __next__


lst = [1, 2, 3, 4, 5, 6]
'''it = lst.__iter__()
print (it.__next__())
print (it.__next__())
print (it.__next__())
print (it.__next__())
print (it.__next__())
print (it.__next__())'''

it = iter(lst)
print (next(it))
print (next(it))
print (next(it))
print (next(it))
print (next(it))
print (next(it))

print (lst[-1])

# lst [a:b:c] a-начало, b-конец, c-шаг

print (lst[1:])
print (lst[:3])
print (lst[::-1])