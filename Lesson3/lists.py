lst1 = []
lst2 = list()

lst3 = [1, 2.5, True, "string", [None, False]]
lst4 = list("hello")
print (lst4)

print (lst3[1])
lst3[2] = "bool"
print (lst3)
del lst3[4]
print (lst3)
print(len(lst3))

print ([1, 2] + [3, 4])
print ([1, 2] * 3)

print (lst3.index("bool"))
(lst3.append([None, False]))
print (lst3)
(lst3.extend([None, False]))
print (lst3)

print (lst3[4][1])
print (lst3[5])

lst = [3, 1, 6, 4, 0]
lst.sort()
print(lst)
lst = lst.sort()
print(lst)
