st1 = set()

st2 = {1, 2, 3, 1, True, 5, 2.5, 0, False}
st3 = set("hello")

print(st2)
print(st3)

st4 = {0, 1, 2, 3}
st5 = {2, 3}
st6 = {2, 3, 4, 5}

print(len(st6))
print(st4 == st6) #False
print(st5 <= st6) #True
print(st5 >= st4) #False
print(st4 | st6) # [0,1,2,3,4,5]
print(st4 & st6) # [2,3]
print(st4 - st6) # [0,1]
print(st4 ^ st6) # [0,1,4,5]
print(st5.issubset(st6)) # == print(st5 <= st6)

st5.add(4)
st4.remove(0)