string1 = ''
string2 = ""
string3 = ''''''
string4 = """"""
string5 = str()

string6 = 'Hello\nWorld!'
string7 = "Hello\nWorld!"
string8 = '''Hello
World!'''
string9 = """Hello
World!"""
string10 = str([10, 20])

print (string9[5])
print (len(string9))
# string9[4] = " "

print ("Hello" + "World")
print ("NO!" * 3)

print (string9.find("ll"))
print (string9.isalpha())
