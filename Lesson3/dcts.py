dct1 = {}
dct2 = dict()

dct3 = {"1": 1, "2": 4, "3": 9} #ключ: значение
dct4 = dict([("1", 1), (2, 4), ("3", 9)]) #список из 3 кортежей из 2 элем. каждый

print(dct3["2"])
print(dct4[2])
dct3["3"] = 10 #изменяет
dct3["4"] = 16 #создает
del dct4[2]

