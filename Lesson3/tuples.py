tpl1 = ()
tpl2 = tuple()

tpl3 = (1, 2.5, None, [False, "string"])
tpl4 = 1, 2.5, None, [False, "string"]
tpl5 = (1,)
tpl6 = 1,
tpl7 = tuple([1, 2])

print (tpl3[3])
print (len(tpl3))

lst = list (tpl3)
print (lst.__sizeof__())
print (tpl3.__sizeof__())

a = 5
b = 6
a, b = b, a

