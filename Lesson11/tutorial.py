import datetime
import sqlalchemy as sa


# URL = "{engine}://{user}:{pass}@{host}/{database}"
URL = "sqlite:///database.sqlite"

engine = sa.create_engine(URL)
metadata = sa.MetaData()

products = sa.Table(
    "products",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("name", sa.String(256), nullable=False, unique=True),
    sa.Column("price", sa.Float, default=lambda: 0.0),
    sa.Column("created_at", sa.DateTime, default=datetime.datetime.now, nullable=False),
)
statistics = sa.Table(
    "statistics",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("product_id", sa.Integer, sa.ForeignKey("products.id")),
    sa.Column("created_at", sa.DateTime, default=datetime.datetime.now, nullable=False),
)
metadata.create_all(engine)

connection = engine.connect()
request = statistics.insert()
connection.execute(request, product_id=1, created_at=datetime.datetime.now())

request = statistics.update().where(statistics.c.id == 1)
connection.execute(request, product_id=3)
