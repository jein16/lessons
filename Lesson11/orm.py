import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


Base = declarative_base()


class Common:
    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)


class Product(Common, Base):
    __tablename__ = "products"

    name = sa.Column(sa.String(64), unique=True, nullable=False)
    price = sa.Column(sa.Float, default=0)


class Statistic(Common, Base):
    __tablename__ = "statistics"
    
    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    product = relationship(Product, uselist=True)
    count = sa.Column(sa.Integer, default=0, nullable=False)
    date = sa.Column(sa.Date, nullable=False, default=datetime.date.today)

    
URL = "sqlite:///database2.sqlite"

engine = sa.create_engine(URL)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

Base.metadata.create_all(engine)

product1 = Product(name="ВодкаКаждый день' .3", price=59.9)
product2 = Product(name="Хлебородински", price=48.90)
session.add(product1)
session.add(product2)
session.commit()

#session.delete(product1)
#session.delete(product2)
#session.commit()

result = session.query(Product).filter(Product.price == 59.9).all()
for product in result:
    print(product.id, product.name, product.price)

#result = session.query(Product).filter(Product.price == 48.9).first()
#result.price = 49.9
#session.commit()

session.close()
