from threading import Thread
from time import sleep

def func1():
    start = 0
    while start < 20:
        print("Func1:", start)
        start += 1
        sleep(1)

def func2():
    start = 5
    while start < 30:
        print("Func2:", start)
        start += 1
        sleep(0.5)

thread1 = Thread(target=func1, args=()) # Создание объектов для потока
thread2 = Thread(target=func2, args=())

print("START")
thread1.start() # Запуск потоков
thread2.start()

start = 0
while start < 10:
    print("MAIN:", start)
    start += 1
    sleep(2)

thread1.join() # Ожидание окончания потока
print("THREAD1 END")
thread2.join()
print("THREAD2 END")
print("END")