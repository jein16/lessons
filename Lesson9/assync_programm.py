import asyncio

@asyncio.coroutine
def function(name, start, end, timeout):
    while start < end:
        print(f"Coroutine {name}: {start}")
        start += 1
        yield from asyncio.sleep(timeout)

loop = asyncio.get_event_loop()
futures = asyncio.gather(
    function("1", 0, 20, 1),
    function("2", 5, 30, 0.5),
)
loop.run_until_complete(futures)
loop.close()