from threading import Lock, Thread
from time import sleep

class MyThread(Thread):
    def __init__(self, name, begin, end, timeoutб daemon=False):
        super().__init__(daemon=daemon)
        self.name = name
        self.begin = begin
        self.end = end
        self.timeout = timeout

    def run(self):
        while self.begin < self.end:
            print(f"Thread {self.name}: {self.begin}")
            self.begin += 1
            sleep(self.timeout)

thread1 = MyThread("1", 0, 20, 1)
thread2 = MyThread("2", 5, 30, 0.5)

print("START")
thread1.start()
thread2.start()

print("END")