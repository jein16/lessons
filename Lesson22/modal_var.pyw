from tkinter import Button, Entry, Label, StringVar, Tk, Toplevel, mainloop


def show_dialog():
    dialog = Toplevel(root)
    entry = Entry(dialog, textvariable=string_var)
    entry.pack()


def render():
    label.config(text=string_var.get())


root = Tk()
string_var = StringVar()
button1 = Button(root, text="Click me!", font="Arial 40", command=show_dialog)
button2 = Button(root, text="Render!", font="Arial 40", command=render)
label = Label(text="EMPTY", font="Arial 40")
label.pack()
button1.pack()
button2.pack()
mainloop()