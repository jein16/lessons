from tkinter import Button, DISABLED, Entry, Label, Radiobutton


def click():
    label.config(text=entry.get())


label = Label(font="ComicSans 20")
entry = Entry(font="ComicSans 20")
button = Button(text="Button", font="ComicSans 20", command=click, state=DISABLED)
radio1 = Radiobutton(text="radiobutton", value=0)
radio2 = Radiobutton(text="radiobutton", value=1)
radio1.pack()
radio2.pack()
label.pack()
entry.pack()
button.pack()
label.mainloop()
