from tkinter import Button, Tk, Toplevel, filedialog, mainloop, messagebox


def show_dialog():
    file_name = filedialog.askopenfilename(
        filetypes=(
            ("TXT files", "*.txt"),
            ("HTML files", "*.htm;*.html"),
            ("ALL files", "*"),
        )
    )
    print(file_name)
    '''top = Toplevel(root)
    top.transient(root)
    top.grab_set()
    top.focus_set()
    top.wait_window()'''
    #print(messagebox.askyesnocancel("Title", "Body")) # или showwarning или showinfo 
    #или askokcancel или askyesno

root = Tk()
button = Button(root, text="Click me, please", command=show_dialog)
button.pack()
mainloop()