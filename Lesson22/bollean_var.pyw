from tkinter import IntVar, Radiobutton, Tk, mainloop


def radio_log():
    print(int_var.get())

tk = Tk()
int_var = IntVar()
radio1 = Radiobutton(text="Value 1", variable=int_var, command=radio_log, value=1)
radio2 = Radiobutton(text="Value 2", variable=int_var, command=radio_log, value=2)
radio3 = Radiobutton(text="Value 3", variable=int_var, command=radio_log, value=3)
radio4 = Radiobutton(text="Value 4", variable=int_var, command=radio_log, value=4)
radio1.pack()
radio2.pack()
radio3.pack()
radio4.pack()
mainloop()