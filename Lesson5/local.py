def func():
    global z
    z = 10
    print("LOCAL:", z)

z = 5
func()
z = 6
func()
print("GLOBAL:", z)



def func1():

    def func2():
        # z = 3
        print("LOCAL:", z)

    z = 2
    func2()
    print("NONLOCAL:", z)

z = 1
func1()
print("GLOBAL:", z)