def edit(lst):
    lst.append(6)
    return lst

lst = [1, 2, 3, 4, 5]
print("BEFORE:", lst)
new_lst = edit(lst)     #edit(lst.copy()) - делает копию
print("AFTER GLOBAL:", lst)
print("AFTER LOCAL:", new_lst)
print("IS IT SAME?", lst is new_lst)