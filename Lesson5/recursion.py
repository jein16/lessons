'''def recursion(level=0):
    print("LEVEL:", level)
    recursion(level + 1)'''


def fibonacci(index):
    if index in (0, 1):
        return index
    return fibonacci(index - 1) + fibonacci(index - 2)

index = int(input())
print(fibonacci(index))