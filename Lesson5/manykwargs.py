def manykwargs(arg1, *args, arg3=20, **kwargs):
    print("ARG1:", arg1)
    print("args:", args)
    print("arg3:", arg3)
    print("kwargs:", kwargs)
    print("-" * 10)

manykwargs(1)
manykwargs(1, 2)
manykwargs(1, 2, 3)                     #args - будет кортежем
manykwargs(1, 2, 3, arg4=50)            #arg4 - будет словарем
manykwargs(1, 2, 3, arg3=80, arg4=100)  #arg4 - будет словарем
