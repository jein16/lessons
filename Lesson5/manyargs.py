def manyargs(arg1, arg2, *args):
    print(args)
    print("-" * 10)

manyargs(1, 2)
manyargs(1, 2, 3, 4)
manyargs(1, 2, 3)
manyargs(1, 2, 3, 4, 5)
