class Animal:       # Создание пустого класса Animal
    pass

# Создадим объекты этого класса

a1 = Animal()       # объект = вызов класса
a2 = Animal()
print(a1.__class__, Animal, a1.__class__ is Animal)
print(isinstance(a1, Animal))       # является ли объект этим классом