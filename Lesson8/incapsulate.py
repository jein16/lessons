class Animal:
    weight = 200            # Статические атрибуты
    color = "red"           # Статические атрибуты

    print("Hello")

a1 = Animal()
a2 = Animal()
print(dir(a1))
print("-" * 30)

print("Animal:", Animal.color, Animal.weight)
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight)
print("-" * 30)

a1.color = "green"
a2.weight = 400

print("Animal:", Animal.color, Animal.weight)
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight)
print("-" * 30)

a1.size = 600
a2.size = 1200

print("Animal:", Animal.color, Animal.weight)
print("A1:", a1.color, a1.weight, a1.size)
print("A2:", a2.color, a2.weight, a2.size)
print(dir(Animal))                 # но в класс атрибуты не записываются
print("-" * 30)

del a1.size
print(dir(a1))

Animal.weight = 50
Animal.color = "blue"
print("Animal:", Animal.color, Animal.weight)
print("A1:", a1.color, a1.weight)
print("A2:", a2.color, a2.weight)
print("-" * 30)