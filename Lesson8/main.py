from calc.ops import div

result = None
try:
    result = div(100, 0)
except ArithmeticError as exception:
    print("GLOBAL 1:", exception.args)
except ZeroDivisionError as exception:
    print("GLOBAL 2:", exception)
print(result)
