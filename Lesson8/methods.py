class Animal:
    weight = 200                       # Статические атрибуты
    color = "red"                      # Статические атрибуты

    # МЕТОДЫ
    def __init__(self, name):          # конструктор вызывается при создании объекта
        print(f"Created {name}")       # он не создает объект
        self.name = name               # динамический атрибут

    def print_weight(self):            # один объект произвольный - self
        print(f"weight {self.name}: {self.weight}")

    def __add__(self, value):
        print("Value:", value)

    def __del__(self):                  # деструктор - вызывается во время удаления объекта
        print(f"Deleted {self.name}")   # он не удаляет объект, удаляет переменную


a1, a2 = Animal("Murka"), Animal("Bor'ka")
a1.weight = 300
a1.print_weight()
a2.print_weight()
Animal.print_weight(a1)
a2 + 5