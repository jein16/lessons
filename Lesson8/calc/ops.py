class MyZeroDivisionError(ZeroDivisionError):
    def __init__(self, number):
        super(Exception, self).__init__(f"number was division by zero")
        self.number = number

def div(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        raise MyZeroDivisionError(a)            # Выброс ошибки