def xrange(start, end, step):
    while start < end:
        yield start             # говорит, что это генератор
        start += step
        
    return "stop!!!"


def fibonacci():
    now, next = 0, 1
    while True:
        yield now
        now, next = next + now, next


gen = xrange(1, 3, 0.5)
print("gen:", gen)

print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
try:
    print(next(gen))
except:
    pass

print("loop: ", end="")
for element in xrange(1, 3, 0.5):
    print(element, end=" ")
print()