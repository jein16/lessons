class Decorator:
    def __new__(cls, _class):
        print("DECORATE")
        return cls

@Decorator
class MyClass:
    def __init__(self):
        print("MY CLASS")


MyClass = Decorator(MyClass)
print("-" * 10)
#mc = MyClass()