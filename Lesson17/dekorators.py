'''def arguments(function):
    def wrapper(*args, **kwargs):
        print(f"ARGS {function.__name__}: {args}")
        print(f"kwargs {function.__name__}: {kwargs}")
        return function(*args, **kwargs)

    return wrapper


def result(function):
    def wrapper(*args, **kwargs):
        result = function(*args, **kwargs)
        print(f"result {function.__name__}: {result}")
        return result

    return wrapper


@result
@arguments              # эквивалентно div = arguments(div) 
def div (a, b):
    return a/ b


print(div(100, b=25))'''








def arguments(function):
    def wrapper(*args, **kwargs):
        print(f"ARGS {function.__name__}: {args}")
        print(f"kwargs {function.__name__}: {kwargs}")
        return function(*args, **kwargs)

    return wrapper


def result(prompt):
    def decorator(function):
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            print(f"{prompt}> result {function.__name__}: {result}")
            return result

        return wrapper

    return decorator

@result("log")
@arguments
def div (a, b):
    return a/ b


div(100, b=25)