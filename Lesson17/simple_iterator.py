class SimpleIterator:
    def __init__(self, count):
        self.now = 0
        self.count = count

    def __next__(self):
        self.now += 1
        if self.now > self.count:
            raise StopIteration

        return self.now


class SimpleIterable:
    def __init__(self, count):
        self.count = count

    def __iter__(self):
        return SimpleIterator(self.count)


it = SimpleIterable(3)
for e1 in it:
    for e2 in it:
        print(e1, e2)