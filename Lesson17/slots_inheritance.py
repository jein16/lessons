class A:
    __slots__ = ("a",)


class B:
    __slots__ = ("b",)


class C(A, B):
    __slots__ = ()
