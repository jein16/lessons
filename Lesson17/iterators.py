lst = [1, 2, 3, 4]
for element in lst:
        print(element, end=", ")
print()

print("__iter__ in lst:", hasattr(lst, "__iter__")) # есть ли у lst метод iter
it = iter(lst)
for element in lst:
    try:
        print(next(it), end=", ")
        print(next(it), end=", ")
    except StopIteration:
        print("it ended, ", end="")
    print(element, end=", ")