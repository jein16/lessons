from timeit import repeat


class Foo:
    __slots__ = ("foo",)


class Bar:
    pass


def get_set_delete(obj):
    obj.foo = "test"
    obj.foo
    del obj.foo

foo = min(repeat(lambda: get_set_delete(Foo())))
bar = min(repeat(lambda: get_set_delete(Bar())))

print(foo, bar)