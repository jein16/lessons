class MyClass:
    __slots__ = ("_value",)

    def set(self, value):
        self._value = value

    def get(self):
        return self._value

my_obj = MyClass()
my_obj.set(20)
print(my_obj.get())

# my_obj.p = 900
del my_obj._value               # он не удалится!
print(dir(my_obj))