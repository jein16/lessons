import requests


#appid = "5d1c5370369029f2d3d9274729db73b2"
appid = "6804f300b5f9bb93ad358a2779a214ef"
def weather(city_name):
    try:
        response = requests.get(
            "http://api.openweathermap.org/data/2.5/weather",
            params={'q': city_name, 'units': 'metric', 'APPID': appid}
            )
        data = response.json()
        deg = data['wind']['deg']
        print(deg)
        print("Temperature:", data['main']['temp'], "C")
        print("Wind:", data['wind']['speed'], "м/с", winds(deg))
    except:
        pass


def winds(deg):
    direct = ['С ','СВ',' В','ЮВ','Ю ','ЮЗ',' З','СЗ']
    for i in range(8):
        step = 45.
        min = i*step - 45/2.
        max = i*step + 45/2.
        if i == 0 and deg > 360-45/2.:
            deg = deg - 360
        if max >= deg >= min:
            direction = direct[i] 
            break
    return direction

print("Input your city:", end=" ")
city_name = input()
weather(city_name)