from tkinter import BOTH, X, Y, Label, Tk, mainloop


root = Tk()
root.geometry("500x500")
#root.resizable(False, False)
label1 = Label(root, width=12, height=6, bg="yellow", text="1", font="Arial 20")
inner_label = Label(label1, width=12, height=6, text="Inner", font="Arial 20")
label2 = Label(root, width=12, height=6, bg="orange", text="2", font="Arial 20")
label3 = Label(root, width=12, height=6, bg="lightgreen", text="3", font="Arial 20")
label4 = Label(root, width=12, height=6, bg="lightblue", text="4", font="Arial 20")
label1.pack(padx=10, ipady=50, anchor="nw")
inner_label.pack()
label2.pack(side="bottom", pady=5, ipadx=10, fill=X)
label3.pack(side="left", padx=10, expand=True)
label4.pack(side="left", ipady=10, expand=True)
mainloop()