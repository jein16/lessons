from tkinter import Canvas, Frame, Tk, mainloop


root = Tk()
frame = Frame(root, width=600, height=600)
canvas = Canvas(frame, width=1000, height=1000)

canvas.place(x=0, y=0)
frame.pack(fill="both", expand=True)

canvas.create_line(10, 10, 190, 50)
canvas.create_line(100, 180, 100, 60, fill="green", width=5, arrow="last",
dash=(10, 2), activefill="red", arrowshape="10 20 10")
canvas.create_rectangle(10, 210, 190, 260)
canvas.create_rectangle(60, 280, 140, 390, fill="yellow", outline="red",
width=3, activedash=(5, 5))
canvas.create_polygon(300, 10, 220, 90, 380, 90)
canvas.create_polygon(240, 110, 360, 110, 390, 180, 210, 180, fill="",
outline="red")
canvas.create_oval(250, 210, 350, 310, width=2)
canvas.create_oval(210, 320, 390, 390, fill="gray70", outline="orange")
canvas.create_oval(410, 10, 590, 190, fill="lightgray", outline="white")
canvas.create_arc(410, 10, 590, 190, start=0, extent=45, fill="red")
canvas.create_arc(410, 10, 590, 190, start=240, extent=100, fill="green",
style="chord")
canvas.create_arc(410, 10, 590, 190, start=160, extent=-70, style="arc",
width=5, outline="darkblue")
canvas.create_text(100, 235, text="Hello, Tk!", justify="center",
font="Verdana 14")

root.mainloop()