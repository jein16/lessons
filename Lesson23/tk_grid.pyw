from tkinter import N, W, E, S, Frame, Label, Tk, mainloop


'''label1 = Label(width=12, height=6, bg="yellow", text="1")
label2 = Label(width=12, height=6, bg="orange", text="2")
label3 = Label(width=12, height=6, bg="orange", text="3")
label4 = Label(width=12, height=6, bg="orange", text="4")
label5 = Label(width=12, height=6, bg="orange", text="5")
label6 = Label(width=12, height=6, bg="orange", text="6")
label7 = Label(width=12, height=6, bg="orange", text="7")
label8 = Label(width=12, height=6, bg="orange", text="8")
label9 = Label(width=12, height=6, bg="orange", text="9")'''

tk = Tk()
LABELS_COLOR = ("Yellow", "orange", "#98FB98", "lightblue",
"white", "red", "blue", "gray", "green")
frame = Frame(tk)
LABELS = [
    Label(frame, width=12, height=6, bg=color, text=str(index)) for index, color in enumerate(LABELS_COLOR)
]
LABELS[0].grid(columnspan=2, padx=10, pady=10)
LABELS[1].grid(row=1, padx=10, pady=10, sticky=N+W)
LABELS[2].grid(row=1, column=1, padx=10, pady=10, sticky=E+W)
LABELS[3].grid(row=1, column=2, padx=10, pady=10, sticky=N+E+S+W)
LABELS[4].grid(row=2, columnspan=2, padx=10, pady=10)
LABELS[5].grid(row=2, column=2, padx=10, pady=10)
LABELS[6].grid(row=3, padx=10, pady=10)
LABELS[7].grid(row=3, column=2, padx=10, pady=10)
LABELS[8].grid(row=4, padx=10, pady=10)
frame.pack()
mainloop()