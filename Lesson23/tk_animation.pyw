from tkinter import Canvas, Frame, Tk, mainloop


root = Tk()
frame = Frame(root)
canvas = Canvas(frame, width=600, height=600)

color = "black"
for angle in range(0, 360, 30):
    canvas.create_arc(0, 0, 600, 600, start=angle, extent=30, fill=color)
    color = "white" if color == "black" else "black"

canvas.place(x=0, y=0)
frame.pack(fill="both", expand=True)

root.mainloop()

