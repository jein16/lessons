f = open("wt.txt", "wt")

f.write("Hello, World!\n")
f.write("Привет, Мир!")
f.close()

# то же самое!

f = open("wt.txt", "wt")

print("Hello, World!", file=f)
print("Привет, Мир!", file=f)
f.close()

##############

f = open("wt.txt", "wt")

f.writelines(["Hello, World!\n", "Привет, Мир!"])
f.close()