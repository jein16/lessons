f = open("plus.txt", "wt+")

f.write("Hello, World!\n")
print(f.tell())                 # говорит на каком месте указатель
f.seek(0)                       # переводит указатель на 1 положение
print(f.tell())
print(f.read())
f.close()

f = open("plus.txt", "rt+")

# f.seek(3)         # перезапишет с 4 позиции   
f.write("POP")      # перезапишет с 1 позиции
print(f.read())
f.write("Привет, Мир!\n")

f.close()