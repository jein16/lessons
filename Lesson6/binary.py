f = open("wb.dat", "wb")

f.write(b"Hello, world!\n")
f.write(bytes((13, 17, 18, 19)))
f.close()

f = open("wb.dat", "rb")

print(f.read())
f.close()


with open("wb.dat", "rb") as f:
    print(tuple(f.read()))          # возврат записанного выше
