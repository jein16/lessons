f = open("./rt.txt")

print(f.read())
print("-" * 40)
f.close()

f = open("./rt.txt")

print(f.read(8))
print(f.read(6))
print("-" * 40)
f.close()

f = open("rt.txt", encoding="cp1251")

print(f.readline())             # читает по строкам
print(f.readline())
print("-" * 40)
f.close()

f = open("rt.txt")

print(f.readline(8))            # читает по строкам по 8
print(f.readline(8))
print(f.readline(8))
print("-" * 40)
f.close()

f = open("rt.txt")

print(f.readlines())            # Создает список из строк
print("-" * 40)
f.close()

f = open("rt.txt")

print(f.readlines(16))
print(f.readlines(8))
print("-" * 40)
f.close()

f = open("rt.txt")
for line in f:
    print(line)
print("-" * 40)
f.close()