import json

obj = {"string": 1, True: 2.5, None: [False]}
#data = json.dumps(obj)
#new_obj = json.loads(data)

#print(new_obj)

with open("obj.json", "wt") as f:
    json.dump(obj, f, indent=4)         # indent - отступ
