import pickle

obj = {"string": 1, True: 2.5, None: [range(20), b"123"]}
print("obj:", obj)

#data = pickle.dumps(obj, protocol=2)            # или просто 2
#print("data:", data)

#new_obj = pickle.loads(data)
#print("new_obj:", new_obj)

with open("obj.pkl", "wb") as f:
    pickle.dump(obj, f)