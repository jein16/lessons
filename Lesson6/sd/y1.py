import yaml

obj = {"string": 1, True: 2.5, None: [False]}

with open("obj.yml", "wt") as f:
    data = yaml.dump(obj, f)