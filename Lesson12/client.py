import socket
from threading import Thread


sock = socket.socket()
print("$ Create socket")
#server_address = ('192.168.2.100', 2000)
server_address = ('localhost', 2000)
sock.connect(server_address)
print(f"$ Connected to {server_address[0]}:{server_address[1]}")

def inn():
    while True: 
        data = sock.recv(1024).decode()
        print("Server > ", data)

def outt():
        while True:
            message = input().encode()
            sock.send(message)

t1 = Thread(target=inn)
t2 = Thread(target=outt)

t1.start()
t2.start()

t1.join()
t2.join()

sock.close()