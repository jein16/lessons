import socket
from threading import Thread


sock = socket.socket()      
print("$ Create socket")
#server_address = ('192.168.2.116', 2000)
server_address = ('localhost', 2000)
sock.bind(server_address)       
print("$ Bind to port 2000")
sock.listen(1)            
print("$ Set listen count to 1")

def inn():
    while True: 
        data = conn.recv(1024).decode() 
        print(f"{addr[0]}:{addr[1]} > {data}")
    conn.close()

def outt():
    while True:
        try:
            message = input().encode()
            conn.send(message)
        except BrokenPipeError:
            break

conn, addr = sock.accept() 
print(f"$ Connect from {addr[0]}:{addr[1]}")
t1 = Thread(target=inn)
t2 = Thread(target=outt)

t1.start()
t2.start()

t1.join()
t2.join()

sock.close()










'''while True:
    conn, addr = sock.accept()  
    print(f"Get connection from {addr[0]} : {addr[1]}")

    while True:
        try:
            data = conn.recv(1024).decode() 
            print(f"{addr[0]}:{addr[1]} > {data}")
            print("Me > ", end="")
            message = input().encode()
            conn.send(message)
        except BrokenPipeError:
            break
        

    conn.close()
    
sock.close()'''