import socket


sock = socket.socket()      # TCP default
print("$ Create socket")
sock.bind(("", 2000))       # от любого адреса и 2000 port
print("$ Bind to port 2000")
sock.listen(1)              # кол-во подключений
print("$ Set listen count to 1")

while True:
    conn, addr = sock.accept()  
    print(f"Get connection from {addr[0]} : {addr[1]}")

    while True:
        try:
            data = conn.recv(1024) 
            print(f"{addr[0]}:{addr[1]} > {data}")
            conn.send(b"Gotcha!")     
        except BrokenPipeError:
            break
        

    conn.close()
    
sock.close()