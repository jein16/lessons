import socket


sock = socket.socket()      # TCP default
print("Create socket")
sock.bind(("", 2000))       # от любого адреса и 2000 port
print("Bind to localhost:2000")
sock.listen(1)              # кол-во подключений
print("Set listen count to 1")

conn, addr = sock.accept()  # Установление соединения с клиентом - какое подключение, с какого адреса
print(f"Get connection from {addr}")

data = conn.recv(1024)      # количество данных до 1024
print(f"Get data from {addr}")
print(data)
conn.send(b"Hello")         # отправить в ответ
print(f"Send response to {addr}")

conn.close()
sock.close()