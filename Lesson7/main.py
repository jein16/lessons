import math

print(dir(math))
print("-" * 40)

print("MATH:", math.sqrt(25))
print("-" * 40)

from math import sin, cos

print("SIN:", sin(0))
print("-" * 40)

from math import asin as _asin

def asin(a):
    return a ** math.e

print("ASIN:", asin(1), "_ASIN:", _asin(1))
print("-" * 40)

from math import *

print("PI:", pi)
print("-" * 40)


import CalcLes as calc

print(dir(calc))
print("-" * 40)

import CalcLes.ops

print("DIV:", CalcLes.ops.div(81, 9))
print("-" * 40)

from CalcLes import ops

print("DIV:", ops.div(100, 10))
print("-" * 40)

from CalcLes.ops import div

print("DIV:", div(121, 11))
print("-" * 40)

import lorm

print(dir(lorm))
print("-" * 40)

import sys

print("sys.path:", sys.path)
print("-" * 40)