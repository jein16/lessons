numbers = list(map(int, input().split()))
n = len(numbers)

for index in range(1, n):
    while index > 0 and numbers[index] < numbers[index - 1]:
        numbers[index], numbers[index - 1] = numbers[index - 1], numbers[index]
        index -= 1
        print("exchange")
    print(numbers)