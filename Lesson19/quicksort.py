#def random():
#    return 42

def quicksort_recursive_pythonway(numbers):
    global it
    it += 1
    if not numbers:
        return numbers

    med = numbers[len(numbers) // 2]
    less = [x for x in numbers if x < med]
    equal = [x for x in numbers if x == med]
    greater = [x for x in numbers if x > med]

    return quicksort_recursive_pythonway(less) + equal + quicksort_recursive_pythonway(greater)


def quicksort_recursive(numbers, left, right):
    global it
    it += 1
    if left >= right:
        return

    L, R = left, right
    med = numbers[(left + right) // 2]

    while L <= R:
        while numbers[L] < med:
            L += 1
        while numbers[R] > med:
            R -= 1

        if L <= R:
            numbers[L], numbers[R] = numbers[R], numbers[L]
            L, R = L + 1, R - 1

    quicksort_recursive(numbers, left, R)
    quicksort_recursive(numbers, L, right)


def quicksort(numbers):
    stack = [(0, len(numbers) - 1)]

    while stack:
        print("STACK:")
        left, right = stack.pop()
        if left >= right:
            continue

        L, R = left, right
        med = numbers[(left + right) // 2]

        while L <= R:
            while numbers[L] < med:
                L += 1
            while numbers[R] > med:
                R -= 1

            if L <= R:
                numbers[L], numbers[R] = numbers[R], numbers[L]
                L, R = L + 1, R - 1

        stack.append((L, right))
        stack.append((left, R))
       



numbers = [int(x) for x in input().split()]
it = 0
print(quicksort_recursive_pythonway(numbers))
print(it)

it = 0
numbers_copy = numbers.copy()
quicksort_recursive(numbers=numbers_copy, left=0, right=len(numbers_copy) - 1)
print(numbers_copy)
print(it)

quicksort(numbers)
print(numbers)