# primer meetup
import sqlalchemy as sa 
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Common:
    id = sa.Column(sa.Integer, primary_key=True)


class Speaker(Common, Base):
    __tablename__ = "speakers"

    name = sa.Column(sa.String(256), nullable=False)
    about = sa.Column(sa.Text, nullable=True)
    phone = sa.Column(sa.String(12), nullable=False)

    
class Speach(Common, Base):
    __tablename__ = "speaches"

    speaker_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Speaker.__tablename__}.id"), nullable=False)
    title = sa.Column(sa.String(64), nullable=False)
    description = sa.Column(sa.Text, nullable=False)
    start_time = sa.Column(sa.DateTimeField, nullable=False)
    end_time = sa.Column(sa.DateTimeField, nullable=False)


class Guest(Common, Base):
    __tablename__ = "guests"

    name = sa.Column(sa.String(256), nullable=False)


#интерфейс для управления БД
class Role:
    def __init__(self):
        self.engine = sa.create_engine("sqlite://database2.sqlite")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_shedule(self):
        return self.session.query(Speach).all()

class SpeakerRole(Role): pass


class GuestRole(Role):
    def register(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()
        self.record = guest

    def cancel(self):
        self.session.delete(self.record)
        self.session.commit


class OrganizerRole(Role):
    def register_guest(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()

    def cancel_guest(self, id):
        guest = self.session.delete(Guest).where(Guest.id == id).first()