import sys

from PyQt5.QtCore import QRect, Qt
from PyQt5.QtGui import QFont, QColor
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow


app = QApplication(sys.argv)

main = QMainWindow()
main.setGeometry(QRect(100, 100, 600, 600))
main.resize(300, 300)

font = QFont()
font.setFamily("Verdana")
font.setPixelSize(25)

label = QLabel("Hello, world!" * 3, main)
label.setFont(font)
label.setStyleSheet("color: #a5a5a5")
label.move(100, 100)
label.adjustSize()
label.setText("Goodbye, world!")

main.show()

sys.exit(app.exec())
