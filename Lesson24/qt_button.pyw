import sys
from time import sleep

from PyQt5.QtWidgets import QApplication, QPushButton, QLineEdit, QMainWindow


def imhere():
    print(lineedit.text())
    sleep(5)


app = QApplication(sys.argv)
main = QMainWindow()

lineedit = QLineEdit(main)
lineedit.resize(400, 40)
lineedit.move(10, 10)

button = QPushButton(main)
button.setText("Click me")
button.resize(100, 20)
button.move(10, 50)
button.clicked.connect(imhere)

main.show()
sys.exit(app.exec())
