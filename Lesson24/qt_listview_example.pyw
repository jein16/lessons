import sys

from PyQt5 import uic
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QApplication, QMainWindow

# from listview import Ui_MainWindow


def add_item():
    text = main.lineEdit.text().strip()
    if text:
        main.listView.model().appendRow(QStandardItem(text))
    main.lineEdit.setText("")


app = QApplication(sys.argv)
# ui = Ui_MainWindow()
main = uic.loadUi("listview_example.ui")
# ui.setupUi(main)
main.pushButton.clicked.connect(add_item)
main.listView.setModel(QStandardItemModel())
main.show()

sys.exit(app.exec_())
