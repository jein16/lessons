import sys

from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QApplication, QListView, QMainWindow, QPushButton


app = QApplication(sys.argv)
main = QMainWindow()
main.move(100, 100)
main.setFixedWidth(500)
main.setFixedHeight(500)

letters = [chr(code) for code in range(65, 91)]

view = QListView(main)
model = QStandardItemModel()
for letter in letters:
    model.appendRow(QStandardItem(str(letter)))
view.setModel(model)
view.resize(400, 400)

main.show()

sys.exit(app.exec_())