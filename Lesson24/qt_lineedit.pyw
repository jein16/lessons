import sys

from PyQt5.QtWidgets import QApplication, QLineEdit, QMainWindow


app = QApplication(sys.argv)
main = QMainWindow()
main.resize(500, 500)
main.move(100, 100)

lineedit1 = QLineEdit(main)
lineedit1.resize(300, 20)
lineedit1.move(20, 20)
lineedit1.setText("Hello, world! " * 2)
lineedit1.setMaxLength(10)
lineedit1.setReadOnly(True)
lineedit1.setStyleSheet("font: ComicSans; font-size: 20px;")
print(lineedit1.text())

lineedit2 = QLineEdit(main)
lineedit2.resize(300, 20)
lineedit2.move(20, 60)

main.show()

sys.exit(app.exec())