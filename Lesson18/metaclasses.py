class MetaClass(type):
    def __new__(self, *args):
        print("Create by MetaClass")
        cls = type(*args)
        cls.__init__(*args)
        return cls

    def __init__(self, *args):
        print("Initialize by MetaClass")


class Class(metaclass=MetaClass):
    pass


print(Class)