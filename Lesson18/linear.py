lst = [int(x) for x in input().split(" ")]
need = int(input())


for index, element in enumerate(lst):
    if element == need:
        print(f"Found in {index}")
        break
else:
    print("Not found")