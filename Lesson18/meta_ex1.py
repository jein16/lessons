class OneParent(type):
    def __new__(cls, name, bases, attributes):
        if len(bases) > 1:
            raise TypeError("inherited multiple base classes")

        return super().__new__(cls, name, bases, attributes)


class Base(metaclass=OneParent): pass
print("Base created")
class A(Base): pass
print("A created")
class B(Base): pass
print("B created")
class C(A, B): pass
print("C created")