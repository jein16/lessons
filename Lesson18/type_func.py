name = "NewClass"
bases = ()
attributes = {
    "a": 1,
    "b": 2,
}

NewClass = type(
    name,
    bases,
    attributes,
)
print(NewClass)
print(NewClass.__dict__)