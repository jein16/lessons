lst = [int(x) for x in input().split(" ")]
lst.sort()
need = int(input())

# СРЕЗЫ из ПИТОНА
iterations = 0
lst_copy = lst.copy()
while lst_copy:
    iterations += 1
    med = len(lst_copy) // 2
    if lst_copy[med] < need:
        lst_copy = lst_copy[med+1:]
    elif lst_copy[med] > need:
        lst_copy = lst_copy[:med]
    else:
        print(f"FOUND for {iterations} iterations")
        break
else:
    print(f"NOT FOUND for {iterations} iterations")


# АЛГОРИТМИЧЕСКИЙ МЕТОД
left = 0
right = len(lst) - 1
iterations = 0
while left <= right:
    iterations += 1
    med = (left + right) // 2
    if lst[med] < need:
        left = med + 1
    elif lst[med] > need:
        right = med - 1
    else:
        print(f"FOUND FOR {iterations} iterations")
        break
else:
    print(f"NOT FOUND for {iterations} iterations")