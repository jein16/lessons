class MetaClass(type):
    _methods = ("print", "check")

    def __new__(cls, name, bases, attributes):
        for method in cls._methods:
            if method not in attributes:
                raise TypeError(f"Class {name} have not {method} method")
        return super().__new__(cls, name, bases, attributes)


class Printer(metaclass=MetaClass):
    def print(self): pass
    def check(self): pass
class APrinter(Printer):
    def print(self): pass
    def check(self): pass
class BPrinter(Printer):
    def print(self): pass