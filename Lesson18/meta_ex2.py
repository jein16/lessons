class EntityMetaClass(type):
    def __new__(cls, name, bases, attributes):
        new_cls = super().__new__(cls, name, bases, attributes)
        new_cls.__del__ = lambda self: print(f"{self.name} deleted")
        return new_cls


class Entity(metaclass=EntityMetaClass):
    def __init__(self, name):
        self.name = name
        print(f"{name} created")


a = Entity("a")
b = Entity("b")