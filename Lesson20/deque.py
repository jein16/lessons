class Deque:
    def __init__(self):
        self._list = []

    def __len__(self):
        return len(self._list)

    def push_first(self, value):
        self._list.insert(0, value)

    def push_last(self, value):
        self._list.append(value)

    def pop_first(self):
        return self._list.pop(0)
    
    def pop_last(self):
        return self._list.pop()


deque = Deque()
deque.push_first(1)
deque.push_first(5)
deque.push_last(3)
deque.push_first(-9)
deque.pop_first()
deque.push_last(20)
deque.pop_first()
deque.pop_last()
deque.pop_first()
deque.push_last(98)
print(deque._list)
        