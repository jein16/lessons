class Queue:
    def __init__(self):
        self._list = []

    def __len__(self):
        return len(self._list)

    def push(self, value):
        self._list.append(value)

    def pop(self):
        return self._list.pop(0)


queue = Queue()
queue.push(1)
queue.push(5)
queue.push(3)
queue.push(-9)
queue.pop()
queue.push(20)
queue.pop()
queue.pop()
queue.pop()
queue.push(98)
print(queue._list)