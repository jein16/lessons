class Stack:
    def __init__(self):
        self._list = []

    def size(self):
        print(len(self._list))
    
    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        dele = self._list.pop()
        print(dele)
    
    def back(self):
        print((self._list[-1]))

    def clear(self):
        self._list.clear()
        print("ok")

    def exit(self):
        print("bye")
        exit()


stack = Stack()
while (True):
    command, *values = input().split()
    func = getattr(stack, command)
    func(*values)

'''stack = Stack()
inp = []
while (True):
    inp = input().split()
    if inp[0] == "push":
        stack.push(inp[1])
    elif inp[0] == "pop":
        stack.pop()
    elif inp[0] == "back":
        stack.back()
    elif inp[0] == "clear":
        stack.clear()
    elif inp[0] == "size":
        stack.__len__()
    elif inp[0] == "exit":
        break
    else:
        continue
print("bye")'''

'''stack = Stack()
stack.push(3)
stack.push(14)
stack.size()
stack.clear()
stack.push(1)
stack.back()
stack.push(2)
stack.back()
stack.pop()
stack.size()
stack.pop()
stack.size()'''
