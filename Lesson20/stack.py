class Stack:
    def __init__(self):
        self._list = []

    def __len__(self):
        return len(self._list)

    def push(self, value):
        self._list.append(value)

    def pop(self):
        return self._list.pop()


stack = Stack()
stack.push(1)
stack.push(5)
stack.push(3)
stack.push(-9)
stack.pop()
stack.push(20)
stack.pop()
stack.pop()
stack.pop()
stack.push(98)
print(stack._list)
