# по-питоновски

graph = {
    "A": ("A", "D"),
    "B": ("A",),
    "C": ("B", "D"),
    "D": ("E",),
    "E": (),
    "F": ("G",),
    "G": ("F",),
}


def depth_recursive(node, target, visited=None):
    print(node)
    if visited is None:
        visited = set()
    visited.add(node)

    if node == target:
        return True

    for vertex in graph[node]:
        if vertex not in visited:
            if depth_recursive(vertex, target, visited):
                return True


    return False


print(depth_recursive("A", "E"))
print(depth_recursive("C", "G"))