class BinaryTree:
    def __init__(self):
        self._tree = {"value": None}

    def push(self, value):
        node = self._tree
        while node["value"] is not None:
            if node["value"] > value:
                node = node["left"]
            elif node["value"] < value:
                node = node["right"]
            else:
                break
        else:
            node["value"] = value
            node["left"] = {"value": None}
            node["right"] = {"value": None}


    def search(self, value):
        iterations = 0
        node = self._tree
        while node["value"] is not None:
            iterations += 1
            if node["value"] > value:
                node = node["left"]
            elif node["value"] < value:
                node = node["right"]
            else:
                print(f"FOUND {value} for {iterations} iterations")
                break
        else:
            print(f"NOT FOUND {value} for {iterations} iterations")


class OptimizeBinaryTree:
    def __init__(self):
        self._tree = []

    def push(self, value):
        index = 0
        element = self._tree[index] if len(self._tree) > index else None
        while element is not None:
            if element > value:
                index = index * 2 + 1
            elif element < value:
                index = index * 2 + 2
            else:
                break
            element = self._tree[index] if len(self._tree) > index else None
        else:
            self._tree.extend([None for _ in range(len(self._tree), index + 1)])
            self._tree[index] = value

    def search(self, value):
        pass


tree = OptimizeBinaryTree()
tree.push(5)
print(tree._tree)
tree.push(4)
print(tree._tree)
tree.push(8)
print(tree._tree)
tree.push(3)
print(tree._tree)
tree.push(6)
print(tree._tree)
tree.push(2)
print(tree._tree)
tree.push(9)
print(tree._tree)
tree.push(1)
print(tree._tree)
tree.push(7)
print(tree._tree)
tree.search(10)
tree.search(4)
tree.search(6)
tree.search(1)