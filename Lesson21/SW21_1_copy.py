a = list()
N_S = []
N_S = list(map(int, input().split()))
'''for i in range(0, N_S[0]):
   k = map(int, input().split())
   if k: a.append(k)
graph = list(list(i) for i in a)'''

graph = [
    [1, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0],
]

n = N_S[0]
s = N_S[1]

visited = [0] * n
def depth_recursive(s):
    visited[s] = 1
    for vertex, value in enumerate(graph[s]):
        if visited[vertex] == 0 and value == 1:
            depth_recursive(vertex)

depth_recursive(s)
print(visited)
print(visited.count(1))