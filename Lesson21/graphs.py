# по-питоновски

graph_python = {
    "A": ("A", "D"),
    "B": ("A",),
    "C": ("B", "D"),
    "D": ("E",),
    "E": (),
    "F": ("G",),
    "G": ("F",),
}


def depth_python_recursive(node, target, visited=None):
    print(node)
    if visited is None:
        visited = set()
    visited.add(node)

    if node == target:
        return True

    for vertex in graph_python[node]:
        if vertex not in visited:
            if depth_python_recursive(vertex, target, visited):
                return True


    return False


def depth_python(start, target):
    visited = set()
    stack = [start]

    while stack:
        vertex = stack.pop()
        print(vertex)
        visited.add(vertex)

        for v in graph_python[vertex]:
            if v == target:
                return True
            elif v not in visited:
                stack.append(v)
            else:
                pass
    
    return False


# матрицы

graph = (
    (1, 0, 0, 1, 0, 0, 0),
    (1, 0, 0, 0, 0, 0, 0),
    (0, 1, 0, 1, 0, 0, 0),
    (0, 0, 0, 0, 1, 0, 0),
    (0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 1),
    (0, 0, 0, 0, 0, 1, 0),
)

def depth_recursive(node, target, visited=None):
    print(node)
    if visited is None:
        visited = set()
    visited.add(node)

    for vertex, value in enumerate(graph[node]):
        if value == 1:
            if vertex == target or (vertex not in visited
            and depth_recursive(vertex, target, visited)):
                return True

    return False


def depth(start, target):
    visited = set()
    stack = [start]

    while stack:
        vertex = stack.pop()
        print(vertex)
        visited.add(vertex)

        for v, value in enumerate(graph[vertex]):
            if value == 1:
                if v == target:
                    return True
                elif v not in visited:
                    stack.append(v)

    return False

print(depth(0, 4))
print(depth(2, 5))