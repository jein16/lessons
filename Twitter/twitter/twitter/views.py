from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import redirect, render, reverse
from django.views import View

from twitter.models import Message


class IndexView(View):
    def get(self, request):
        messages = Message.objects.all()
        return render(request, "index.html", {"messages": messages})

    def post(self, request):
        message = Message(
            text=request.POST["text"],
            author=request.user,
        )
        message.save()
        return redirect(reverse("index"))


class AuthView(View):
    def get(self, request):
        return render(request, "auth.html")

    def post(self, request):
        if request.POST["action"] == "signin":
            return self.signin(request)
        elif request.POST["action"] == "signup":
            return self.signup(request)

    def signin(self, request):
        user = authenticate(
            username=request.POST["username"],
            password=request.POST["password"],
        )
        #if user is not None:
        #    print("User is authenticated")
        #    return redirect(reverse("index"))
        #else:
        #    print("The username and password were incorrect")
        #    return redirect(reverse("auth"))
        login(request, user)
        return redirect(reverse("index"))

    def signup(self,request):
        user = User.objects.create_user(
            username=request.POST["username"],
            password=request.POST["password"],
        )
        login(request, user)
        return redirect(reverse("index"))

class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect(reverse("index"))