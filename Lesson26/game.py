from random import randint

import pygame


YELLOW = (255, 255, 0)
RED = (255, 0, 0)


class Leprecon(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()

        self.image = pygame.image.load("./mario.png")
        self.image = pygame.transform.scale(self.image, (width, height))

        self.rectangle = pygame.Rect(x, y, width, height)

        self.speed = (0, 0)
        self.speed_multiply = 10
        self.right = True


    def handle(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    self.speed = (0, 1 * self.speed_multiply)
                elif event.key == pygame.K_UP:
                    self.speed = (0, -1 * self.speed_multiply)                   
                elif event.key == pygame.K_LEFT:
                    self.speed = (-1 * self.speed_multiply, 0)
                    if self.right:
                        self.image = pygame.transform.flip(self.image, True, False)
                        self.right = False
                elif event.key == pygame.K_RIGHT:
                    self.speed = (1 * self.speed_multiply, 0)
                    if not self.right:
                        self.image = pygame.transform.flip(self.image, True, False)
                        self.right = True

    
    def update(self):
        condition = self.rectangle.right <= 800 or self.speed[0] <= 0
        condition = condition and (self.rectangle.left >= 0 or self.speed[0] >= 0)
        condition = condition and (self.rectangle.top >= 10 or self.speed[1] >= 0)
        condition = condition and (self.rectangle.bottom <= 790 or self.speed[1] <= 0)


        if condition:
            self.rectangle = self.rectangle.move(*self.speed)


class Coin(pygame.sprite.Sprite):
    def __init__(self, leprecon, x, y, width, height):
        super().__init__()

        self.leprecon = leprecon

        self.image = pygame.image.load("./coin.png")
        self.image = pygame.transform.scale(self.image, (width, height))

        self.rectangle = pygame.Rect(x, y, width, height)

        self.sound = pygame.mixer.Sound("./coin.wav")


    def update(self):
        if self.leprecon.rectangle.colliderect(self.rectangle):
            self.sound.play()
            self.rectangle = pygame.Rect(
            randint(10, 740),
            randint(10, 740),
            self.rectangle.width,
            self.rectangle.height,
        )
            return True
        return False


class Menu(pygame.sprite.Sprite):
    def __init__(self, leprecon, game):
        super().__init__()

        self.leprecon = leprecon
        self.pause = False
        self.game = game

        self.button1 = pygame.Rect(300, 340, 200, 50)
        self.button2 = pygame.Rect(300, 410, 200, 50)

        self.button3 = pygame.Rect(300, 520, 200, 50)

    
    def handle(self, events):
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                if self.button1.collidepoint(x, y):
                    self.leprecon.speed_multiply -= 1
                elif self.button2.collidepoint(x, y):
                    self.leprecon.speed_multiply += 1
                elif self.button3.collidepoint(x, y):
                    self.game.play = False


class Game:
    def __init__(self, fps = 60, show_fps=False):
        pygame.init()

        self.leprecon = Leprecon(x=350, y=700, width=100, height=100)
        self.coin = Coin(x=randint(10, 740), y=randint(10,740), width=50,
        height=50, leprecon = self.leprecon)
        self.menu = Menu(self.leprecon, self)

        self.points = 0
        self.play = True
        self.fps = fps
        self.clock = pygame.time.Clock()

        self.font = pygame.font.Font(None, 36)
        self.screen = pygame.display.set_mode((800, 800), pygame.FULLSCREEN)


    def loop(self):
        while self.play:
            self.draw()
            self.clock.tick(self.fps)
            self.handle()
            self.update()


    def draw(self):
        self.screen.fill((0, 0, 0))

        if self.menu.pause:
            pygame.draw.rect(self.screen, RED, self.menu.button1)
            pygame.draw.rect(self.screen, RED, self.menu.button2)
            pygame.draw.rect(self.screen, RED, self.menu.button3)   

            text = self.font.render("- speed", 0, (255, 255, 255))
            self.screen.blit(text, (self.menu.button1.x, self.menu.button1.y))

            text = self.font.render("+ speed", 0, (255, 255, 255))
            self.screen.blit(text, (self.menu.button2.x, self.menu.button2.y))

            text = self.font.render("quit", 0, (255, 255, 255))
            self.screen.blit(text, (self.menu.button3.x, self.menu.button3.y))
        else:
            self.screen.blit(self.leprecon.image, self.leprecon.rectangle)        
            self.screen.blit(self.coin.image, self.coin.rectangle) 
            pygame.draw.rect(self.screen, (128, 128, 128), (0, 0, 800, 800), 10)

            points_text = self.font.render(f"Points: {self.points}", 0, (0, 255, 0))
            self.screen.blit(points_text, (600, 0))

        pygame.display.update()


    def handle(self):
        events = pygame.event.get()

        if not self.menu.pause:
            self.leprecon.handle(events)
        else:
            self.menu.handle(events)

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.menu.pause = not self.menu.pause


    def update(self):
        if not self.menu.pause:
            self.leprecon.update()
            if self.coin.update():
                self.points += 1
            

game = Game()
game.loop()