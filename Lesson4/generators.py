lst = []
for i in range(0, 11):
    lst.append(i)
print(lst)

lst = [x ** 2 for x in range(0, 100001)]
print(lst.__sizeof__())

st = {x ** 2 for x in range (0, 100001)}
print(st.__sizeof__())

tpl = (x ** 2 for x in range (0, 100001))  # неизменяемый - кортеж, так что это ГЕНЕРАТОР!!!
print(tpl.__sizeof__())

dct = {x: x ** 2 for x in range (0,100001)}
print(dct.__sizeof__())

for i in tpl:
    print(i)