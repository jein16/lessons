while True:
    answer = input()
    if answer == "exit":
        break
    print(answer)
print("END")

answer = ""
while answer != "exit":
    answer = input()
    if answer == "quit":
        break
    print(answer)
else:
    print("break wasn't called")


lst = [5, 8, 3, 4, 9]
found = 6
for element in lst:
    if element == found:
        print(found)
        break
else:
    print("NOT FOUND")

found = 4
for element in lst:
    if element == found:
        print("found")
        break
else:
    print("NOT FOUND")


