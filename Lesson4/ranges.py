lst = [5, 8, 1, 3, 4]

index = 0
while index < len(lst):
        print(index, "=>", lst[index])
        index += 1
print("-" * 40)

for index in range(len(lst)):
    print(index, "=>", lst[index])
print("-" * 40)

for index, element in enumerate({"1": 1, "2": 2}):      # кортеж из index и element
    print(repr(index), "->", repr(element))
print("-" * 40)

for index, element in enumerate(lst):
    print(index, "->", element)
print("-" * 40)

