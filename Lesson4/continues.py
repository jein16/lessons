lst = [5, 8, 1, 3, 4]
for element in lst:
    if element in (8, 4):               # либо 8, либо 4
        continue
    print(element)

count = 10
while count > 0:
    count -= 1
    if count in (4, 8):
        continue
    print(count)
